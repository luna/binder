defmodule Binder.Ring do
  use GenServer
  alias HashRing.ETS

  require Logger

  @moduledoc """
  Main Binder Ring module.
  """

  def start_link(opts \\ []) do
    Logger.info("Binder.Ring: starting hash ring")

    # start the 3 rings
    {:ok, ring_pid} = ETS.start_link(BinderRing)
    {:ok, external_ring} = ETS.start_link(ExternalRing)
    {:ok, cache_ring} = ETS.start_link(CacheRing)

    Registry.start_link(keys: :unique, name: :binder_reg)

    # register rings
    Registry.register(:binder_reg, :ring, ring_pid)
    Registry.register(:binder_reg, :ext_ring, external_ring)
    Registry.register(:binder_reg, :cache_ring, cache_ring)

    {:ok, ring_pid}
  end

  def get_anyr(ring) do
    [{_pid, ring_pid}] = Registry.lookup(:binder_reg, ring)
    ring_pid
  end

  def get_ring do
    get_anyr(:ring)
  end

  def get_ering do
    get_anyr(:ext_ring)
  end

  def get_cring do
    get_anyr(:cache_ring)
  end

  def get_worker(id) do
    get_anyr("worker_#{id}")
  end

  def get_eworker_s(id) do
    "ext_worker_#{id}"
  end

  def get_cworker_s(id) do
    "cache_worker_#{id}"
  end

  def get_eworker(id) do
    get_anyr("ext_worker_#{id}")
  end

  def get_cworker(id) do
    get_anyr("cache_worker_#{id}")
  end

  def init(:ok) do
    {:ok, nil}
  end
end
