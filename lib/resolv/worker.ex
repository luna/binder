defmodule Binder.ResolvWorker do
  use GenServer

  alias Binder.Ring
  alias Binder.External
  alias HashRing.ETS
  alias Binder.CacheWorker

  require Logger

  @moduledoc """
  Main resolv worker

  This module defines the main worker
  that resolves DNS questions.
  """

  def start_link(worker_id) do
    GenServer.start_link(__MODULE__, worker_id, [])
  end

  def query(pid, pkt_rd, domain, qtype, sockdata) do
    GenServer.call(pid, {:query, pkt_rd, domain, qtype, sockdata})
  end

  def make_bucket(prefix, addr) do
    case addr do
      {p1, p2, p3, p4} ->
        "#{prefix}:#{p1}_#{p2}_#{p3}_#{p4}"

      {p1, p2, p3, p4, p5, p6, p7, p8} ->
        "#{prefix}:#{p1}_#{p2}_#{p3}_#{p4}_#{p5}_#{p6}_#{p7}_#{p8}"
    end
  end

  def init(worker_id) do
    Logger.debug(fn ->
      "resolv worker start_link: #{inspect(self())}"
    end)

    ring_pid = Ring.get_ring()

    case ETS.add_node(ring_pid, worker_id) do
      {:ok, _nodes} ->
        :ok

      # this happens when the old worker died but
      # the PID is still in the ring, making the new,
      # restarting worker to fail when adding itself
      # to the ring. this takes care of that.
      {:error, :node_exists} ->
        {:ok, _nodes} = ETS.remove_node(ring_pid, worker_id)
        {:ok, _nodes} = ETS.add_node(ring_pid, worker_id)
    end

    Registry.register(:binder_reg, "worker_#{worker_id}", self())
    {:ok, nil}
  end

  def keyfunc({:domain, domain, qtype}) do
    "#{domain}:#{qtype}"
  end

  def find_rs(rs) do
    rootservers = Application.fetch_env!(:binder, :root_servers_ipv4)

    Enum.find(rootservers, fn record ->
      {rs_name, _ttl, _rec, ip} = record
      rs_name == rs
    end)
  end

  @doc """
  Make an external query to an external worker,
  given the IP Address of a DNS server to query,
  the domain and the qtype we want to find.
  """
  defp ext_query(ip, domain, qtype) do
    {:ok, worker_id} = ETS.find_node(ExternalRing, domain)
    worker = Ring.get_eworker(worker_id)
    External.query(worker, ip, domain, qtype)
  end

  @doc """
  Get the IP for a nameserver.
  """
  defp get_ns_rr(ns_rr_ar) do
    ref_domain = extract_rr_ns(ns_rr_ar)

    # NOTE: maybe search :aaaa when you want an ipv6?
    ns_rr =
      case resolve(true, ref_domain, :a) do
        {:error, err} ->
          {:error, err}

        {:ans, [answer | _others]} ->
          answer

        {:ok, _qdlist, anlist, _nslist, _arlist} ->
          Enum.random(anlist)
      end

    case ns_rr do
      {:error, err} ->
        {:error, err}

      {:dns_rr, _domain, :a, _in, _cnt, _ttl, next_ip, _rr_tm, _rr_bm, _rr_func} ->
        next_ip
    end
  end

  @doc """
  Find the referral's ip given the arlist on the request.
  """
  defp find_ref_ip(arlist, ref_domain) do
    Enum.find(arlist, fn rr ->
      {rr_domain, rr_qtype, rr_ip} = extract_rr(rr)

      # NOTE: add :aaaa here for ipv6
      rr_domain == ref_domain and rr_qtype == :a
    end)
  end

  @doc """
  Fetch the IP address, given ref_ip.

  If ref_ip belongs to a root server, we search
  that in our configuration. if not, we call find_ref_ip/2.
  """
  defp root_or_name(arlist, ref_ip) do
    upper_ref_ip =
      ref_ip
      |> to_string
      |> String.upcase()

    is_rootserver =
      upper_ref_ip
      |> String.contains?("ROOT-SERVERS.NET")

    if is_rootserver do
      ip = find_rs(upper_ref_ip)
      {:dns_rr, upper_ref_ip, nil, nil, nil, nil, ip, nil, nil, nil}
    else
      find_ref_ip(arlist, ref_ip)
    end
  end

  @doc """
  Extract useful information out of a RR.
  """
  defp extract_rr(rr) do
    {:dns_rr, domain, qtype, :in, _cnt, _ttl, ip, _tm, _bm, _func} = rr
    {domain, qtype, ip}
  end

  defp extract_rr_ns(rr) do
    {:dns_rr, tld, :ns, :in, _cnt, _ttl, nameserver, _tm, _bm, _func} = rr
    nameserver
  end

  defp extract_qtype(rr) do
    {:dns_rr, _d, qtype, :in, _cnt, _ttl, _d2, _tm, _bm, _func} = rr
    qtype
  end

  defp match_ns(ref, arlist, nslist) do
    ref_domain = extract_rr_ns(ref)

    addr_ns =
      Enum.find(arlist, fn rr ->
        {rr_domain, rr_qtype, rr_ip} = extract_rr(rr)

        # TODO: add :aaaa for ipv6
        rr_domain == ref_domain and rr_qtype == :a
      end)

    if addr_ns == nil do
      []
    else
      [addr_ns]
    end
  end

  defp find_nameservers(nslist, arlist) do
    good_nslist =
      nslist
      |> Enum.flat_map(fn ref ->
        qtype = extract_qtype(ref)

        if qtype == :ns do
          match_ns(ref, arlist, nslist)
        else
          []
        end
      end)

    t_nslist =
      nslist
      |> Enum.filter(fn ref ->
        {:dns_rr, _1, qtype, :in, _2, _3, _4, _5, _6, _7} = ref
        qtype == :ns
      end)

    reftype =
      cond do
        not Enum.empty?(good_nslist) -> :refs
        Enum.empty?(t_nslist) -> :empty
        true -> :nonempty
      end

    {reftype, good_nslist}
  end

  defp extract_anlist(reply) do
    case reply do
      {:ans, anlist_r} -> anlist_r
      {:ok, _1, anlist_r, _3, _4} -> anlist_r
      {:error, err} -> []
    end
  end

  defp check_nameservers(t_nslist, arlist, ctx) do
    {{qdlist, anlist, nslist, arlist}, {domain, qtype}, reply, data, recursion_count} = ctx

    case find_nameservers(t_nslist, arlist) do
      {:empty, []} ->
        # this happens when nslist is empty.
        case data do
          {:error, err} ->
            {:error, err}

          _ ->
            r_anlist = extract_anlist(reply)

            {
              :ok,
              qdlist,
              # merge our answers and remove duplicates
              (r_anlist ++ anlist) |> Enum.uniq(),
              nslist,
              arlist
            }
        end

      {:nonempty, []} ->
        # no nameservers were found with addresses on arlist,
        # but nslist isn't empty. so we can get a random
        # nameserver off nslist, and query for its address.
        ns = Enum.random(t_nslist)
        next_ip = get_ns_rr(ns)
        follow_next(next_ip, domain, qtype, recursion_count)

      {:refs, refs} ->
        # we got a nameserver(s) that has its address on arlist,
        # then we get one of them at random and follow the path
        #
        # NOTE: using a random reference actually causes problems
        #  with nameservers that don't actually have the domain info
        #  (like amazons dns configuration)

        # ref = Enum.random refs
        ref = Enum.at(refs, 0)
        {ref_domain, ref_qtype, ref_ip} = extract_rr(ref)

        case ref_qtype do
          :soa ->
            # SOA records usually mean the domain was not found,
            # but didn't actually send to us an NXDOMAIN error.
            {:ok, qdlist, anlist, nslist, arlist}

          _any ->
            follow_next(ref_ip, domain, qtype, recursion_count)
        end
    end
  end

  defp insert_cache({d_name, d_qtype}, anlist) do
    # for each answer, we put it in cache
    Enum.each(anlist, fn answer ->
      {:dns_rr, name, qtype, :in, _cnt, ttl, _data, _tm, _bm, _func} = answer

      {:domain, name, qtype}
      |> keyfunc
      |> CacheWorker.put({:ans, [answer]}, ttl)
    end)

    # store the entire anlist in cache as well,
    # TODO: maybe we use the largest ttl in the record list?
    {:domain, d_name, d_qtype}
    |> keyfunc
    |> CacheWorker.put({:ans, anlist}, 120)

    :ok
  end

  defp make_reply(anlist, domain, qtype) do
    case anlist do
      {:error, err} ->
        {:error, err}

      [] ->
        # as soon as we reach somewhere where we dont have any referrals
        # and the answers are nothing, this domain doesnt exist (...?)
        {:error, :not_found}

      # match against a cname
      [{:dns_rr, _domain, :cname, :in, _cnt, _ttl, target, _tm, _bm, _func} = cname_rec | _rest] =
          res ->
        if qtype != :cname do
          # follow the CNAME record when we don't want it,
          # embedding the cname record inside our response in the end.
          case resolve(true, target, qtype) do
            {:error, err} ->
              {:error, err}

            {:ans, anlist} ->
              {:ans, [cname_rec | anlist]}

            {:ok, qdlist, anlist, nslist, arlist} ->
              {:ok, qdlist, [cname_rec | anlist], nslist, arlist}
          end
        else
          # this is a CNAME answer, so return
          # just the list of answers.
          {:ans, anlist}
        end

      _any ->
        insert_cache({domain, qtype}, anlist)
        {:ans, anlist}
    end
  end

  defp follow_next(next_ip, domain, qtype, recursion_count) do
    case next_ip do
      {:error, err} ->
        {:error, err}

      answer ->
        # recrsively follow the refferal path
        call_recursive(next_ip, domain, qtype, recursion_count + 1)
    end
  end

  @doc """
  Call DNS recursively.

  The idea is call that ip, DON'T SET THE RA BIT.
  Get a *referral* reply, then call this function again,
  until we get something that isn't a referral reply!
  """
  def call_recursive(_ip, _domain, _qtype, 15) do
    # if we recursive too much, stop
    {:error, :max_recursion}
  end

  def call_recursive(ip, domain, qtype, recursion_count \\ 0) do
    data = ext_query(ip, domain, qtype)

    {qdlist, anlist, nslist, arlist} =
      case data do
        {:error, err} ->
          {[], {:error, err}, [], []}

        {:dns_rec, _header, qdlist, anlist, nslist, arlist} ->
          {qdlist, anlist, nslist, arlist}
      end

    # make our reply before anything
    reply = make_reply(anlist, domain, qtype)

    # if any replies were found, we should
    # not enter nslist, at all.
    t_nslist =
      case anlist do
        {:error, err} -> []
        [] -> nslist
        _ -> []
      end

    check_nameservers(t_nslist, arlist, {
      {qdlist, anlist, nslist, arlist},
      {domain, qtype},
      reply,
      data,
      recursion_count
    })
  end

  def call_cache_check_cname(domain, qtype) do
    if qtype == :a do
      cached_cname = {:domain, domain, :cname} |> keyfunc |> CacheWorker.get()

      if cached_cname == nil do
        :not_found
      else
        {:ans, anlist} = cached_cname
        cname_rr = Enum.at(anlist, 0)
        {_domain, _qtype, address} = extract_rr(cname_rr)

        reply =
          case resolve(true, address, :a) do
            {:error, err} ->
              {:error, err}

            {:ans, reply_anlist} ->
              {:ans, [cname_rr | reply_anlist]}

            {:ok, qdlist, r_anlist, nslist, arlist} ->
              {:ok, [{:dns_query, domain, qtype, :in}], [cname_rr | r_anlist], nslist, arlist}
          end

        {:ok, reply}
      end
    else
      :not_found
    end
  end

  def call_cache(domain, qtype) do
    cached =
      {:domain, domain, qtype}
      |> keyfunc
      |> CacheWorker.get()

    if cached == nil do
      call_cache_check_cname(domain, qtype)
    else
      {:ok, cached}
    end
  end

  @doc """
  Bootstrap call_recursive calls by
  feeding it with a root server.
  """
  defp start_recursive(domain, qtype) do
    # TODO: ipv6 when?
    roots = Application.fetch_env!(:binder, :root_servers_ipv4)

    # select a random root server to start with
    rootserv = Enum.random(roots)
    {_name, _ttl, _type, root_ip} = rootserv
    {:ok, root_tup} = :inet.parse_address(root_ip)

    call_recursive(root_tup, domain, qtype)
  end

  def do_recursive(domain, qtype) do
    case call_cache(domain, qtype) do
      {:ok, cached} -> cached
      :not_found -> start_recursive(domain, qtype)
    end
  end

  defp do_local_recursive(domain, qtype) do
    {requests, sec} = Application.fetch_env!(:binder, :local_ratelimit)

    case Hammer.check_rate("local_recursive", sec * 1000, requests) do
      {:allow, _count} ->
        do_recursive(domain, qtype)

      {:deny, _limit} ->
        {:error, :ratelimited}

      {:error, err} ->
        Logger.error("error on do_local_recursive check_rate: #{inspect(err)}")
        {:error, err}
    end
  end

  def start_iterative(domain, qtype) do
    roots = Application.fetch_env!(:binder, :root_servers_ipv4)

    # random root server
    {orig_str, _ttl, _qt, orig_ip} = Enum.random(roots)

    # convert into good types for packet encoding
    ref_str = orig_str |> String.to_charlist()
    {:ok, ref_ip} = orig_ip |> :inet.parse_address()

    {
      :ok,
      [],
      [],

      # nslist
      [{:dns_rr, domain, :ns, :in, 0, 172_800, ref_str, :undefined, [], false}],

      # arlist
      [{:dns_rr, ref_str, :a, :in, 0, 172_800, ref_ip, :undefined, [], false}]
    }
  end

  def do_iterative(domain, qtype) do
    case call_cache(domain, qtype) do
      {:ok, cached} -> cached
      :not_found -> start_iterative(domain, qtype)
    end
  end

  def handle_resolve(pkt_rd, domain, qtype, sockdata, ext_flag \\ true) do
    # check our forwarders and our RA config
    recursive = Application.fetch_env!(:binder, :recursive)

    if pkt_rd and recursive do
      # get recursive ratelimit
      {requests, sec} = Application.fetch_env!(:binder, :ratelimit)
      {_sock, addr, _port} = sockdata

      bucket = make_bucket("recursive", addr)

      case {ext_flag, Hammer.check_rate(bucket, sec * 1000, requests)} do
        {false, _any} ->
          do_local_recursive(domain, qtype)

        {true, {:allow, _count}} ->
          do_recursive(domain, qtype)

        {true, {:deny, _limit}} ->
          do_iterative(domain, qtype)

        {true, {:error, err}} ->
          Logger.error("error on check_rate:handle resolve = #{inspect(err)}")
          {:error, err}
      end
    else
      do_iterative(domain, qtype)
    end
  end

  defp handle_resolve(pkt_rd, domain, qtype) do
    handle_resolve(pkt_rd, domain, qtype, {nil, {0, 0, 0, 0}, nil}, false)
  end

  @doc """
  Internal resolve function.
  Called by the worker (for the "recursive" part of DNS).
  """
  def resolve(pkt_rd, domain, qtype) do
    cached =
      {:domain, domain, qtype}
      |> keyfunc
      |> CacheWorker.get()

    if cached == nil do
      handle_resolve(pkt_rd, domain, qtype)
    else
      cached
    end
  end

  @doc """
  External resolve function. Called by external UDP clients.
  """
  def ext_resolve(pkt_rd, domain, qtype, sockdata) do
    # get global ratelimit
    {requests, sec} = Application.fetch_env!(:binder, :gl_ratelimit)
    {_sock, addr, _port} = sockdata
    bucket = make_bucket("ext_resolv", addr)

    case Hammer.check_rate(bucket, sec * 1000, requests) do
      {:allow, _count} ->
        handle_resolve(pkt_rd, domain, qtype, sockdata)

      {:deny, _limit} ->
        {:error, :ratelimited}

      {:error, err} ->
        Logger.error("error on ext_resolve check_rate: #{inspect(err)}")
        {:error, err}
    end
  end

  def handle_call({:query, pkt_rd, domain, qtype, sockdata}, _from, state) do
    Logger.info("Resolving #{domain}, #{inspect(qtype)} record")
    {:reply, ext_resolve(pkt_rd, domain, qtype, sockdata), state}
  end

  def handle_info({from, {:error, err}}, state) do
    {:noreply, state}
  end
end
