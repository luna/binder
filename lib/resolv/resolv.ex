defmodule Binder.Resolv do
  require Logger
  alias HashRing.ETS
  alias Binder.Ring
  alias Binder.ResolvWorker

  def query(pkt_rd, domain, qtype, _qclass, sockdata) do
    {:ok, worker_id} = ETS.find_node(BinderRing, domain)
    worker = Ring.get_worker(worker_id)

    try do
      # pleas like me
      ResolvWorker.query(worker, pkt_rd, domain, qtype, sockdata)
    catch
      # bad idea by catching the exit signal, but its
      # the only option, since this can actually timeout
      # and we don't want to crash the caller
      :exit, _ -> {:error, :timeout}
    end
  end
end
