defmodule Binder.ResolvSupervisor do
  use Supervisor

  require Logger

  def start_link(opts) do
    Supervisor.start_link(__MODULE__, :ok, opts)
  end

  def init(:ok) do
    workers = Application.fetch_env!(:binder, :resolv_workers)
    Logger.info("Binder.ResolvSupervisor: #{workers} workers")

    Logger.debug(fn ->
      "resolv supervisor: #{inspect(self())}"
    end)

    children =
      Enum.map(1..workers, fn id ->
        worker_id = "resolv_#{id}"

        %{
          id: worker_id,
          start: {Binder.ResolvWorker, :start_link, [worker_id]}
        }
      end)

    Logger.debug(fn ->
      "#{inspect(children)}"
    end)

    opts = [
      strategy: :one_for_one,
      name: __MODULE__,
      max_restarts: 10,
      max_seconds: 1
    ]

    Supervisor.init(children, opts)
  end
end
