defmodule Binder.External do
  use GenServer

  alias Binder.Ring
  alias HashRing.ETS

  require Logger

  @moduledoc """
  External query module.

  This worker module describes a GenServer
  that is used to query external DNS resolvers
  for a specific query.

  This module can only solve a single question, and
  will not follow CNAMEs / references to other places,
  since that is the job of ResolvWorker.
  """

  def start_link(worker_id, opts \\ []) do
    GenServer.start_link(__MODULE__, worker_id, opts)
  end

  def query(pid, ip, domain, qtype) do
    {requests, sec} = Application.fetch_env!(:binder, :local_ratelimit)

    case Hammer.check_rate("ext_resolv", sec * 1000, requests) do
      {:allow, _count} ->
        GenServer.call(pid, {:resolv, ip, domain, qtype})

      {:deny, _limit} ->
        {:error, :ratelimited}

      {:error, err} ->
        Logger.warn("ext_query ratelimit err: #{inspect(err)}")
        {:error, err}
    end
  end

  def init(worker_id) do
    # register myself inside ring
    ering_pid = Ring.get_ering()
    {:ok, _nodes} = ETS.add_node(ering_pid, worker_id)

    worker_str = Ring.get_eworker_s(worker_id)
    Registry.register(:binder_reg, worker_str, self())

    {:ok, %{}}
  end

  defp do_socket(ip, domain, qtype, from, socket, state) do
    # construct our request packet
    header = {
      :dns_header,
      # id
      123,
      # qr
      0,
      # opcode
      0,
      # aa
      0,
      # tc
      0,
      # rd (no recursion pls)
      0,
      # ra
      0,
      # pr (??)
      0,
      # rcode
      0
    }

    packet = {
      :dns_rec,
      header,
      [{:dns_query, domain, qtype, :in}],
      [],
      [],
      []
    }

    data = :inet_dns.encode(packet)

    # send packet to a dns server
    :gen_udp.send(socket, ip, 53, data)

    # do a timeout after some time
    Process.send_after(self(), {:do_timeout, {domain, qtype}}, 4500)

    # map current socket to the client
    {:noreply, Map.put(state, {domain, qtype}, {from, socket})}
  end

  def handle_call({:resolv, ip, domain, qtype}, from, state) do
    Logger.info("ext query, #{inspect(ip)} #{inspect(domain)} #{inspect(qtype)}")

    # attach a random port to us
    port = :rand.uniform(65_535)

    # since gen_udp can indeed fail on us
    Process.send_after(self(), {:raw_timeout, from}, 4500)

    udp_res =
      case ip do
        {_a, _b, _c, _d} -> :gen_udp.open(port, [:binary])
        {_a, _b, _c, _d, _e, _f, _g, _h} -> :gen_udp.open(port, [:binary, :inet6])
      end

    # udp_res = :gen_udp.open(port, [:binary])

    # uncomment this to test supervisors
    # Process.sleep(6000)

    case udp_res do
      {:ok, socket} ->
        # do our request to the dns server
        do_socket(ip, domain, qtype, from, socket, state)

      {:error, :eaddrinuse} ->
        # retry
        Logger.warn("got EADDRINUSE, retrying!")
        handle_call({:resolv, ip, domain, qtype}, from, state)

      {:error, err} ->
        Logger.warn("external: Got an error while opening UDP @ port #{port}")
        {:reply, {:error, err}, state}
    end
  end

  defp handle_response(data, socket, state) do
    {:dns_rec, _header, qdlist, anlist, nslist, arlist} = data
    [query | _query] = qdlist

    {:dns_query, domain, qtype, qclass} = query

    Logger.debug(fn ->
      """
      external query res:
        qdlist: #{inspect(qdlist)}
        anlist: #{inspect(anlist)}
        nslist: #{inspect(nslist)}
        arlist: #{inspect(arlist)}
      """
    end)

    client_key = {domain, qtype}
    clisock = Map.get(state, client_key)

    # do our reply
    if clisock != nil do
      {client, socket} = clisock
      GenServer.reply(client, data)

      :gen_udp.close(socket)

      # drop off current client as "clients waiting for reply"
      {:noreply, Map.delete(state, client_key)}
    else
      Logger.warn("got a reply to unknown client. k=#{inspect(client_key)}")

      :gen_udp.close(socket)
      {:noreply, state}
    end
  end

  def handle_info({:udp, socket, _ip, _port, data}, state) do
    # got a reply from the dns server
    case :inet_dns.decode(data) do
      {:error, err} ->
        Logger.warn("external: had issue on the reply: #{inspect(err)}")

        # close the current socket since we don't need it anymore
        # we also can't close the client inside the map, sooo
        :gen_udp.close(socket)
        {:noreply, state}

      {:ok, dns_rec} ->
        handle_response(dns_rec, socket, state)
    end
  end

  def handle_info({:do_timeout, ckey}, state) do
    client_data = Map.get(state, ckey)

    if client_data != nil do
      {from, socket} = client_data

      Logger.warn("timeout!!! #{inspect(from)}")
      :gen_udp.close(socket)
      GenServer.reply(from, {:error, :timeout})
    end

    {:noreply, Map.delete(state, ckey)}
  end

  def handle_info({:raw_timeout, from}, state) do
    GenServer.reply(from, {:error, :timeout})
    {:noreply, state}
  end
end
