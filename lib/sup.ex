defmodule Binder.Supervisor do
  use Supervisor

  require Logger

  def start_link(opts) do
    Supervisor.start_link(__MODULE__, :ok, opts)
  end

  def init(:ok) do
    Logger.info("Binder.Supervisor: starting main sup")

    children =
      [
        {
          DynamicSupervisor,
          [strategy: :one_for_one, name: Binder.ConnSupervisor]
        },
        Binder.Ring,
        Binder.CacheSupervisor,
        Binder.Server,
        Binder.ExternalSupervisor,
        Binder.ResolvSupervisor
      ]
      |> Enum.map(fn atom ->
        # make it global so we can inspect them on iex
        if is_atom(atom) do
          {atom, name: {:global, atom}}
        else
          atom
        end
      end)

    Logger.debug(inspect(children))
    Supervisor.init(children, strategy: :one_for_one)
  end
end
