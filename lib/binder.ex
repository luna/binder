defmodule Binder do
  use Application

  require Logger

  def start(_type, _args) do
    Logger.info("Binder: starting application")

    Supervisor.start_link(
      [
        {Binder.Supervisor, name: {:global, Binder.Supervisor}}
      ],
      name: {:global, Binder.MainSupervisor},
      strategy: :one_for_one
    )
  end
end
