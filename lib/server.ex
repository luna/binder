defmodule Binder.Server do
  use GenServer

  alias Binder.CacheWorker
  alias Binder.ResolvWorker
  alias Binder.Resolv

  require Logger

  @moduledoc """
  This is the main module which accepts connections
  from the UDP socket and spawns Binder.Connection
  handlers for the socket.

  There can be only one Binder.Server running.
  """

  def start_link(opts) do
    GenServer.start_link(__MODULE__, :ok, opts)
  end

  def init(:ok) do
    port = Application.fetch_env!(:binder, :server_port)

    {:ok, _socket} = :gen_udp.open(port, [:binary])
    Logger.info("Binder.Server: udp start at port #{inspect(port)}")

    # initialize custom mappings
    mappings = Application.fetch_env!(:binder, :custom_mappings)

    Enum.each(mappings, fn mapping ->
      {qtype, domain, ip} = mapping

      {:domain, domain, qtype}
      |> ResolvWorker.keyfunc()
      |> CacheWorker.put(
        {:ans,
         [
           {:dns_rr, domain, qtype, :in, 0, 300, ip, :undefined, [], false}
         ]},
        :infinite
      )
    end)

    server_ra = Application.fetch_env!(:binder, :recursive)

    {:ok,
     %{
       server_ra: server_ra
     }}
  end

  def get_err_val(err) do
    Logger.warn("err! #{inspect(err)}")

    err_val =
      case err do
        :fmt -> 1
        :max_recursion -> 2
        :timeout -> 2
        :not_found -> 3
        :invalid_network -> 4
        :ratelimited -> 5
        any -> 2
      end
  end

  defp send_complete_error({socket, addr, port}, {pkt_id, pkt_opcode, pkt_rd, qdlist}, err, state) do
    err_val = err |> get_err_val

    payload = {
      :dns_rec,
      {
        :dns_header,
        pkt_id,
        1,
        pkt_opcode,
        false,
        false,
        pkt_rd,
        state.server_ra,
        0,
        # ERROR BIT SET HERE
        err_val
      },
      qdlist,
      [],
      [],
      []
    }

    data = :inet_dns.encode(payload)
    :gen_udp.send(socket, addr, port, data)
  end

  defp send_error({socket, addr, port}, err, state) do
    err_val = err |> get_err_val

    payload = {
      :dns_rec,
      {
        :dns_header,
        # how do?
        0,
        1,
        0,
        false,
        false,
        0,
        state.server_ra,
        0,
        # ERROR BIT SET HERE
        err_val
      },
      [],
      [],
      [],
      []
    }

    data = :inet_dns.encode(payload)
    :gen_udp.send(socket, addr, port, data)
  end

  defp solve_question(question, sockdata, {_pkt_id, _pkt_opcode, pkt_rd, qdlist}) do
    {:dns_query, domain, qtype, :in} = question

    # domain is a **bitstring**, not a string.
    # qtype can be any of :a, :mx, etc.

    # true hell happens here
    case Resolv.query(pkt_rd, domain, qtype, :in, sockdata) do
      {:ans, anlist} ->
        # got data? good!
        {:ok, qdlist, anlist, [], []}

      {:ok, w_qdlist, anlist, nslist, arlist} ->
        # all the data the worker has
        {:ok, w_qdlist, anlist, nslist, arlist}

      {:error, err} ->
        Logger.warn("Error resolving domain. #{inspect(err)} #{inspect(question)}")
        {:error, err}

      any ->
        Logger.warn("Received.. what? #{inspect(any)}")
        any
    end
  end

  defp solve_questions_reduce(
         tup,
         {:ok, cur_qdlist, cur_anlist, cur_nslist, cur_arlist},
         {sockdata, ctx}
       ) do
    case tup do
      {:error, err} ->
        send(self(), {:error, err, sockdata, ctx})

      {:ok, acc_qdlist, acc_anlist, acc_nslist, acc_arlist} ->
        {:ok, cur_qdlist ++ acc_qdlist, cur_anlist ++ acc_anlist, cur_nslist ++ acc_nslist,
         cur_arlist ++ acc_arlist}
    end
  end

  defp solve_questions(qdlist, sockdata, ctx) do
    qdlist
    |> Enum.map(fn question ->
      {:dns_query, _, _, network} = question

      if network == :in do
        solve_question(question, sockdata, ctx)
      else
        {:error, :invalid_network}
      end
    end)
    |> Enum.reduce(fn tup, cur ->
      case cur do
        # if getting a propagated error, continue it
        {:error, err} ->
          {:error, err}

        {:ok, _1, _2, _3, _4} ->
          # not propagating error, catch tup stuff
          solve_questions_reduce(tup, cur, {sockdata, ctx})
      end
    end)
  end

  @doc """
  This function handles the DNS packet from a client.
  """
  def handle_packet(
        {
          {socket, addr, port},
          {:dns_rec, header, qdlist, _a, _n, _ar}
        },
        state
      ) do
    # extract header information
    {:dns_header, pkt_id, pkt_qr, pkt_opcode, pkt_aa, pkt_tc, pkt_rd, pkt_ra, pkt_pr, rcode} =
      header

    # build our reply from the client's questions
    ans_reply =
      qdlist |> solve_questions({socket, addr, port}, {pkt_id, pkt_opcode, pkt_rd, qdlist})

    # check if any of the reply is an error and if yes, dispatch the packet
    # with an error accordingly.
    {a_qdlist, a_anlist, a_nslist, a_arlist, is_error} =
      case ans_reply do
        {:error, err} ->
          Logger.warn("err: #{inspect(err)} packet id = #{pkt_id}")

          send_complete_error(
            {socket, addr, port},
            {pkt_id, pkt_opcode, pkt_rd, qdlist},
            err,
            state
          )

          {qdlist, [], [], [], true}

        {:ok, a_qdlist, a_anlist, a_nslist, a_arlist} ->
          {a_qdlist, a_anlist, a_nslist, a_arlist, false}
      end

    Logger.debug(fn ->
      """
      reply:
        qdlist: #{inspect(a_qdlist)}
        anlist: #{inspect(a_anlist)}
        nslist: #{inspect(a_nslist)}
        arlist: #{inspect(a_arlist)}
      """
    end)

    # if no error happened, we send
    # a proper reply to the client.
    if not is_error do
      packet =
        {:dns_rec,
         {
           :dns_header,
           pkt_id,
           # response!
           1,
           pkt_opcode,
           # authoritative answer
           false,
           # truncation flag
           false,
           # recursion desired
           pkt_rd,
           # recursion available
           state.server_ra,
           # z
           0,
           # rcode (error code)
           0
         }, a_qdlist, a_anlist, a_nslist, a_arlist}

      data = :inet_dns.encode(packet)
      :gen_udp.send(socket, addr, port, data)
    end

    {:noreply, state}
  end

  def handle_info({:udp, socket, addr, port, data}, state) do
    case :inet_dns.decode(data) do
      {:ok, dns_rec} ->
        # use Task.start instead of spawn/1
        Task.start(fn ->
          handle_packet({{socket, addr, port}, dns_rec}, state)
        end)

        {:noreply, state}

      {:error, err} ->
        # failed to parse the message
        send_error({socket, addr, port}, err, state)
        {:noreply, state}
    end
  end
end
