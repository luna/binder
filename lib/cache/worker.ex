defmodule Binder.CacheWorker do
  use GenServer
  require Logger
  alias HashRing.ETS
  alias Binder.Ring

  @moduledoc """
  Describe a cache worker.
  """

  def start_link(worker_id) do
    GenServer.start_link(__MODULE__, worker_id)
  end

  def get(key) do
    {:ok, worker_id} = ETS.find_node(CacheRing, key)

    worker_pid = Ring.get_cworker(worker_id)
    GenServer.call(worker_pid, {:get, key})
  end

  def put(key, data, ttl) do
    {:ok, worker_id} = ETS.find_node(CacheRing, key)

    worker_pid = Ring.get_cworker(worker_id)
    GenServer.call(worker_pid, {:put, key, data, ttl})
  end

  def init(worker_id) do
    # register inside ring
    cring_pid = Ring.get_cring()
    {:ok, _nodes} = ETS.add_node(cring_pid, worker_id)

    worker_str = Ring.get_cworker_s(worker_id)
    Registry.register(:binder_reg, worker_str, self())

    {:ok,
     %{
       cache: %{}
     }}
  end

  def handle_call({:get, key}, _from, state) do
    raw = Map.get(state.cache, key)

    Logger.debug(fn ->
      "Binder.CacheWorker: GET #{key} is good? #{raw != nil}"
    end)

    if raw == nil do
      {:reply, nil, state}
    else
      {t_end, data} = Map.get(state.cache, key)
      tstamp = :erlang.monotonic_time(:seconds)

      delta =
        case t_end do
          :infinite ->
            -1

          ts_end when is_number(ts_end) ->
            tstamp - t_end
        end

      Logger.debug(fn ->
        "end timestamp: #{t_end}, remaining delta: #{delta}"
      end)

      # if we still have time left
      if delta < 0 do
        {:reply, data, state}
      else
        new_state = Map.delete(state.cache, key)

        Logger.debug(fn ->
          "Binder.CacheWorker: GET #{key} has TTL expired!"
        end)

        {:reply, nil, %{state | cache: new_state}}
      end
    end
  end

  def handle_call({:put, key, value, ttl}, _from, state) do
    tstamp = :erlang.monotonic_time(:seconds)

    tend =
      case ttl do
        :infinite -> :infinite
        num -> tstamp + ttl
      end

    Logger.debug(fn ->
      "Binder.CacheWorker: PUT #{key} ttl=#{ttl} -> #{inspect(value)}"
    end)

    new_cache = Map.put(state.cache, key, {tend, value})
    {:reply, :ok, %{state | cache: new_cache}}
  end
end
