defmodule Binder.CacheSupervisor do
  use Supervisor

  require Logger

  def start_link(opts) do
    Supervisor.start_link(__MODULE__, :ok, opts)
  end

  def init(:ok) do
    workers = Application.fetch_env!(:binder, :cache_workers)
    Logger.info("Binder.CacheSupervisor: #{workers} workers")

    children =
      Enum.map(1..workers, fn id ->
        worker_id = to_string(id)

        %{
          id: worker_id,
          start: {Binder.CacheWorker, :start_link, [worker_id]}
        }
      end)

    Supervisor.init(children, strategy: :one_for_one)
  end
end
