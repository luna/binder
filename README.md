# Binder

Binder is a dns server written in elixir. Very experimental.

**NOTE:** This server does NOT follow all the relevant RFCs on DNS. It
implements DNS-over-UDP, and has some ratelimiting, that's it.

## Features

- Elixir! (`gen_udp` is great)
- Uses a hash ring with workers to solve dns queries
- Nicer configuration than BIND9! :DDDDDDD

## Installation

- Install the **latest** version of Elixir first [here](https://elixir-lang.org/install.html).
- Binder has been tested on OTP 23, Elixir 1.10 (check with `elixir -v`)

```bash
mix deps.get
mix compile
```

## Configuration

Copy `config/config.exs.example` to `config/config.exs` and edit accordingly.

## Running

```bash
iex -S mix

# or
mix run --no-halt
```
