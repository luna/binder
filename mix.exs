defmodule Binder.MixProject do
  use Mix.Project

  def project do
    [
      app: :binder,
      version: "0.1.0",
      elixir: "~> 1.10",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      mod: {Binder, []},
      extra_applications: [:ex_hash_ring, :logger]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:ex_hash_ring, github: "discordapp/ex_hash_ring"},
      {:hammer, "~> 5.0"}
    ]
  end
end
